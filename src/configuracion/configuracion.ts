import { registerAs } from "@nestjs/config";
 
export default registerAs('ejemplo', () => ({
    variable1: process.env.variable1,
    variable2: process.env.variable2,
    variable3: process.env.variable3
}))