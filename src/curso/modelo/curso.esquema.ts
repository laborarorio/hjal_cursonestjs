import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type CursoDocument = Curso & Document;

@Schema()
export class Curso {
    @Prop({require : true})
    titulo:string;
    
    @Prop({require : true})
    precio:number;

    @Prop()
    cover:string;

    @Prop()
    descripcion:string;
}
export const cursoEsquema = SchemaFactory.createForClass(Curso);