import { Controller, Get, Post, Body, Patch, Param, Delete, HttpCode, HttpException, HttpStatus, ParseIntPipe, UseInterceptors } from '@nestjs/common';
import { CursoService } from './curso.service';
import { CreateCursoDto } from './dto/create-curso.dto';
import { UpdateCursoDto } from './dto/update-curso.dto';
import { ApiTags } from '@nestjs/swagger';
import { ExceptionsHandler } from '@nestjs/core/exceptions/exceptions-handler';

@ApiTags('curso')
@Controller('curso')
export class CursoController {
  constructor(private readonly cursoService: CursoService) {}

  @Post()
  @HttpCode(201)
  create(@Body() createCursoDto: CreateCursoDto) {
    // const {precio} =createCursoDto;
    // if (precio===50) throw new HttpException('El precio es muy barato',HttpStatus.FORBIDDEN)
    try{
        console.log("__moneda___",process.env.CURRENCY);
        return this.cursoService.create(createCursoDto);
       
    }catch(e){
        throw new HttpException('Invalida',HttpStatus.FORBIDDEN);
    }
  }

  @Get()
  findAll() {
    return this.cursoService.findAll();
  }

  @Get(':id')
  findOne(@Param('id',new ParseIntPipe ({
    errorHttpStatusCode:HttpStatus.NOT_ACCEPTABLE
  })) id: number) {
    return this.cursoService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateCursoDto: UpdateCursoDto) {
    return this.cursoService.update(+id, updateCursoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.cursoService.remove(+id);
  }
}
