import { Module } from '@nestjs/common';
import { CursoService } from './curso.service';
import { CursoController } from './curso.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { cursoEsquema ,Curso } from './modelo/curso.esquema';

@Module({
  imports:[
    MongooseModule.forFeature(
      [
        {name:Curso.name ,schema:cursoEsquema}
      ]
    )
  ],
  controllers: [CursoController],
  providers: [CursoService],
})
export class CursoModule {}
