import { Injectable } from '@nestjs/common';
import { CreateCursoDto } from './dto/create-curso.dto';
import { UpdateCursoDto } from './dto/update-curso.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Curso, CursoDocument } from './modelo/curso.esquema';
import { Model } from 'mongoose';

@Injectable()
export class CursoService {
  constructor(
    @InjectModel(Curso.name) private readonly cursoModel:Model<CursoDocument>
  ){
    
  }
  create(createCursoDto: CreateCursoDto) {
    return this.cursoModel.create(createCursoDto);
    //return 'Esta acciónn agrega un nuevo curso';
  }

  findAll() {
    return `This action returns all curso`;
  }

  findOne(id: number) {
    return `This action returns a #${id} curso`;
  }

  update(id: number, updateCursoDto: UpdateCursoDto) {
    return `This action updates a #${id} curso`;
  }

  remove(id: number) {
    return `This action removes a #${id} curso`;
  }
}
