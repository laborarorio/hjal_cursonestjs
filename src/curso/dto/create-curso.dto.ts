import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUrl } from "class-validator"

export class CreateCursoDto {
    
    @ApiProperty()
    @IsNotEmpty()
    titulo:string;

    @ApiProperty()
    @IsNotEmpty()
    precio:number;

    @ApiProperty()
    @IsNotEmpty()
    @IsUrl()
    cover:string;

    @ApiProperty()
    @IsNotEmpty()
    descripcion:string

}
