import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe, VersioningType } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { json } from 'express';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  app.use(json({limit:'60mb'}));
  app.enableVersioning({
  defaultVersion:'1',
  type:VersioningType.URI,});
  const config = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('Documentacion API del curso')
    .setDescription('Descripcion de los metodos del curso')
    .setVersion('1')
    .addTag('curso')
    .addTag('videos')
    .addTag('awards')
    .addTag('auth')
    .build();
  const document = SwaggerModule.createDocument(app, config);
    SwaggerModule.setup('documentacion', app, document);
    console.log("Puerto_de_Ambiente_",process.env.PORT);
    console.log("Ambiente_",process.env.NODE_ENV);
    await app.listen(3000);
}
bootstrap();
