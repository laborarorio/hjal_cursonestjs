import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CursoModule } from './curso/curso.module';
import { VideosModule } from './videos/videos.module';
import { AwardsModule } from './awards/awards.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import configuracion from './configuracion/configuracion';
import { EjemploModule } from './modulos/ejemplo/ejemplo.module';
import { UsuariosModule } from './usuarios/usuarios.module';
import { MongooseModule } from '@nestjs/mongoose';
require('dotenv').config()

@Module({
  imports: [
    ConfigModule.forRoot({
        load:[configuracion],
        envFilePath:`./env/${process.env.NODE_ENV}.env`,
        isGlobal:true 
    }),
     ServeStaticModule.forRoot({
       rootPath: join(__dirname, '..', 'publico'),
     }),
    MongooseModule.forRoot(process.env.DB_URI),
    CursoModule,
    AuthModule,
    VideosModule,
    AwardsModule,
    EjemploModule,
    UsuariosModule
      ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
