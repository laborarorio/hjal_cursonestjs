import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AppService {
  constructor(private configService:ConfigService){}  
  getHello(): string {
    return 'Hola Mundo Nestjs';
  }
  getDespedida(): string {
    return 'Adiós chicos del Nestjs';
  }
}
