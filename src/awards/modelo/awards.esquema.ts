import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type AwardsDocument = Awards & Document;

@Schema()
export class Awards {
    @Prop({require : true})
    titulo:string;
    
    @Prop()
    idUsuario:mongoose.Types.ObjectId;

    @Prop()
    descripcion:string;
    
}
export const AwardsEsquema = SchemaFactory.createForClass(Awards);