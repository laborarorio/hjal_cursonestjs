import { DataSource } from 'typeorm';

export const databaseProviders = [
    {
      provide: 'DATA_SOURCE',
      useFactory: async () => {
        const dataSource = new DataSource({
          type: 'postgres',
          host: '172.21.30.13',
          port: 5432,
          username: 'postgres',
          password: 'D3s4rr011o**',
          database: 'postgres',
          entities: [
              __dirname + '/../**/*.entity{.ts,.js}',
          ],
          synchronize: true,
        });
  
        return dataSource.initialize();
      },
    },
  ];