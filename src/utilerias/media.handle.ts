import { diskStorage } from 'multer';

export const almacenar = diskStorage({

    destination:`./temporal`,
    filename: (req, archivo, cb) => {
        const extension = archivo.originalname.split('.').pop();
        const nombrearchivo = `${Date.now()}.${extension}`;
        cb(null,nombrearchivo);
    }
});