import { Module } from '@nestjs/common';
import { EjemploService } from './ejemplo.service';
import { EjemploController } from './ejemplo.controller';

@Module({
  providers: [EjemploService],
  controllers: [EjemploController]
})
export class EjemploModule {}
