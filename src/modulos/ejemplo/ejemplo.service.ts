import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class EjemploService {
    constructor(private configService: ConfigService){
        console.log(this.configService.get('ejemplo.variable1'));
        console.log(this.configService.get('ejemplo.variable2'));
        console.log(this.configService.get('ejemplo.variable3'));
    }
}
