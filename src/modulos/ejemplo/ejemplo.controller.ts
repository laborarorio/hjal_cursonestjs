import { Controller, Get } from '@nestjs/common';
import { EjemploService } from './ejemplo.service';

@Controller('ejemplo')
export class EjemploController {
    constructor(private readonly EjemploService: EjemploService) {}
}
