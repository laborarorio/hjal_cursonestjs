
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

export type VideosDocument = Videos & Document;

@Schema()
export class Videos {
    @Prop({require : true})
    titulo:string;
    
    @Prop()
    idCurso:mongoose.Types.ObjectId;

    @Prop({require : true})
    recurso:string;

    @Prop()
    score:number;

    @Prop()
    descripcion:string;
    
}
export const VideosEsquema = SchemaFactory.createForClass(Videos);