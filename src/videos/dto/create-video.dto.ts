import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty,Length,IsUrl } from "class-validator";

export class CreateVideoDto {
    @ApiProperty()
    @IsNotEmpty()
    @Length(1,30)
    Titulo: string;

    @ApiProperty()
    @IsNotEmpty()
    @Length(1,50)
    Descripcion: string;

   @ApiProperty()
    @IsNotEmpty()
    Categoria: string;

   @ApiProperty()
    @IsUrl()
    url: string


}
