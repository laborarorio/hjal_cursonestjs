import { Controller, Get, Post, Body, Patch, Param, Delete, Query, UseInterceptors, UploadedFile } from '@nestjs/common';
import { VideosService } from './videos.service';
import { CreateVideoDto } from './dto/create-video.dto';
import { UpdateVideoDto } from './dto/update-video.dto';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { LoggerInterceptor } from 'src/utilerias/logger.interceptor';
import { FileInterceptor } from '@nestjs/platform-express';
import { almacenar } from 'src/utilerias/media.handle';

@ApiTags('videos')
@Controller('videos')
@UseInterceptors(LoggerInterceptor)
@ApiBearerAuth()
export class VideosController {
  constructor(private readonly videosService: VideosService) {}

  @Post()
  create(@Body() createVideoDto:CreateVideoDto) {
     console.log(createVideoDto)
    return this.videosService.create(createVideoDto);
  }
  @Post('subir')
  @UseInterceptors(FileInterceptor('avatar',{storage:almacenar}))
  handleUpload(@UploadedFile() archivo:Express.Multer.File){
    console.log(archivo)
  }

  @Get() // REALIZA http://localhost:3000/videos?id=1 & descripcion= holamundo parametro
  findAll(@Query() consulta:string) {
    //console.log(consulta)
    return this.videosService.findAll();
  }

  @Get(':id') //realiza http://localhost:3000/videos/video-1
  findOne(@Param('id') id: string) {
    return this.videosService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateVideoDto: UpdateVideoDto) {
    return this.videosService.update(+id, updateVideoDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.videosService.remove(+id);
  }
}
