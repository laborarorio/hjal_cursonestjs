import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';


export type UsuariosDocument = Usuarios & Document;

@Schema({timestamps:true})
export class Usuarios {
    @Prop({require : true, unique:true})
    email:string;
    
    @Prop({require : true})
    contraseña:string;

    @Prop()
    avatar:string;

    @Prop()
    descripcion:string;
}
export const UsuariosEsquema = SchemaFactory.createForClass(Usuarios);